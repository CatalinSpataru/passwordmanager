﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsumerApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PasswordManager.Data;
using PasswordManager.Models;

namespace PasswordManager.Controllers
{
    public class AccountsController : Controller
    {
        private readonly PasswordManagerContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private const string _encryptionApiLink = @"https://localhost:44315/";
        private const string _decryptionApiLink = @"https://localhost:44312/";
        private const string _encryptMethod = "PasswordEncoder";
        private const string _decryptMethod = "PasswordDecoder";
        private const string _valueParam = "value";
        private const string _dateParam = "date";

        public AccountsController(PasswordManagerContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Accounts
        public async Task<IActionResult> Index()
        {
            var loggedUser = await _userManager.GetUserAsync(User);
            return View(await _context.Account.Where(a => a.IdentityUser.Id == loggedUser.Id).ToListAsync());
        }

        // GET: Accounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // GET: Accounts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Accounts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApplicationName,Username,Password")] Account account)
        {
            if (ModelState.IsValid)
            {
                var loggedUser = await _userManager.GetUserAsync(User);
                account.IdentityUser = loggedUser;

                string dateTimeOfCreation = "2020-22-22";

                string apiRequestUrl = string.Format($"{_encryptionApiLink + _encryptMethod}?{_valueParam}={account.ApplicationName}&{_dateParam}={dateTimeOfCreation}");

                var initialize = new ApiRequest<string, string>(apiRequestUrl, null, account.Password, RestSharp.Method.POST, null);
                string encryptedPassword = initialize.GetData().GetAwaiter().GetResult();

                if (!string.IsNullOrEmpty(encryptedPassword))
                    account.Password = encryptedPassword;

                _context.Add(account);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(account);
        }

        private string DecryptPassword(string appName, string dateOfCreation, string passToDecrypt)
        {
            string decryptedPassword = "";

            string apiRequestUrl = string.Format($"{_decryptionApiLink + _decryptMethod}?{_valueParam}={appName}&{_dateParam}={dateOfCreation}");

            var initialize = new ApiRequest<string, string>(apiRequestUrl, null, passToDecrypt, RestSharp.Method.GET, null);
            string encryptedPassword = initialize.GetData().GetAwaiter().GetResult();

            if (!string.IsNullOrEmpty(encryptedPassword))
                decryptedPassword = encryptedPassword;
            return decryptedPassword;
        }

        // GET: Accounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }
            return View(account);
        }

        // POST: Accounts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ApplicationName,Username,Password")] Account account)
        {
            if (id != account.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(account);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccountExists(account.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(account);
        }

        // GET: Accounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Account
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // POST: Accounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var account = await _context.Account.FindAsync(id);
            _context.Account.Remove(account);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccountExists(int id)
        {
            return _context.Account.Any(e => e.Id == id);
        }
    }
}
