﻿using DecryptionService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DecryptionService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PasswordDecoderController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get(string value, string date, [FromBody] string passToDecode)
        {
            string result = "";

            try
            {
                var keyForDecryption = KeyGenerator.GenerateKeyFromStrings(value, date);

                result = PasswordDecoder.Decrypt(passToDecode, keyForDecryption);
            }
            catch { }

            return Ok(result);
        }
    }
}
