﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DecryptionService.Models
{
    public class KeyGenerator
    {
        public KeyGenerator() { }

        public static string GenerateKeyFromStrings(string first, string second)
        {
            int nrOfCharsInFirst = first.Length - 1;
            int nrOfCharsInSecond = second.Length - 1;

            string generatedKey = "";

            while (nrOfCharsInFirst >= 0 && nrOfCharsInSecond >= 0)
            {
                if (nrOfCharsInFirst >= 0)
                {
                    generatedKey += first[nrOfCharsInFirst];
                    nrOfCharsInFirst --;
                }
                if (nrOfCharsInSecond >= 0)
                {
                    generatedKey += second[nrOfCharsInSecond];
                    nrOfCharsInSecond--;
                }
            }
            
            return generatedKey;
        }
    }
}
