﻿using EncryptionService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EncryptionService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PasswordEncoderController : ControllerBase
    {
        [HttpPost]
        public ActionResult<string> Post(string value, string date, [FromBody] string passToEncode)
        {
            string result = "";
            try
            {
                var keyForEncryption = KeyGenerator.GenerateKeyFromStrings(value, date);

                result = PasswordEncoder.Encrypt(passToEncode, keyForEncryption);
            }
            catch { }

            return Ok(result);
        }
    }
}
